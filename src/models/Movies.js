const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de mascotas
const movieSchema = new Schema(
  {
    title: { type: String, required: true },
    director: { type: String },
    year: { type: Date},
    description: { type: String },
    gender: { type: String, required: true },
  },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Movie
const Movie = mongoose.model('Movie', movieSchema);
// Exportamos el modelo
module.exports = Movie;