const express = require('express');
const Movie = require('../src/models/Movies');

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const movies = await Movie.find();
		return res.status(200).json(movies)
	} catch (err) {
		return res.status(500).json(err);
	}
});


router.get("/:id", async (req, res) => {
	const id = req.params.id;
	try {
		  const movie = await Movie.findById(id);
		  return res.status(200).json(movie);
	} catch (err) {
		  return res.status(500).json(err);
	}
});


router.get("/gender/:gender", async (req, res) => {
	const gender = req.params.gender;
  
	try {
		  const movie = await Movie.find({gender: gender});
	  return res.status(200).json(movie);
	  } catch (err) {
		  return res.status(500).json(err);
	  }
});

router.get("/title/:title", async (req, res) => {
	const title = req.params.title;
  
	try {
		  const movie = await Movie.find({title: title});
	  return res.status(200).json(movie);
	  } catch (err) {
		  return res.status(500).json(err);
	  }
});


router.get("/year/:year", async (req, res) => {
	const year = req.params.year;
  
	try {
		  const movie = await Movie.find({ year: {"$lte": year}});
	  return res.status(200).json(movie);
	  } catch (err) {
		  return res.status(500).json(err);
	  }
});


// POST
router.post('/', async (req, res, next) => {
	// No podremos hacerlo a través del navegador, ya que necesitaríamos un formulario o una función fetch que envíe datos en formato JSON, así que vamos a utilizar Postman para simular peticiones.
	try {
	  // Crearemos una instancia de mascota con los datos enviados
	  const newMovie = new Movie({
		title: req.body.title,
		director: req.body.director,
		year: req.body.year,
		description: req.body.description,
		gender: req.body.gender
	  });
  
	  // Guardamos  en la DB
	  const createdMovie = await newMovie.save();
	  if (!createdMovie){
		return res.status(500).json({message: "No se ha podido añadir la nueva pelicula"});
	  }

	  return res.status(200).json(createdMovie);
	  
	} catch (err) {
		  // Lanzamos la función next con el error para que gestione todo Express
	  next(err);
	}
});



// PUT
router.put('/', async (req, res, next) => {
	try {
	  const id = req.body.id;
  
	  const updatedPet = await Movie.findByIdAndUpdate(
		id, // La id para encontrar el documento a actualizar
		{ title: req.body.title }, // Campos que actualizaremos
		{ new: true } // Usando esta opción, conseguiremos el documento actualizado cuando se complete el update
	  );

	  if (!updatedPet){
		return res.status(500).json({message: "No se ha podido actualizar el documento en la colección Movie"});
	  }
  
	  return res.status(200).json(updatedPet);
	} catch (err) {
	  next(err);
	}
});


// DELETE
// Aunque podamos encontrarlo en algún proyecto, no es correcto enviar body (como en post o put) a nuestro endpoit de tipo DELETE, 
// por lo tanto utilizaremos un parámetro de ruta /:id para identificar el elemento que queremos eliminar de nuestra DB.
router.delete('/delete/:id', async (req, res, next) => {
	try {
	  const id = req.params.id;
		  
		  // No será necesaria asignar el resultado a una variable ya que vamos a eliminarlo
	  const deletePet = await Movie.findByIdAndDelete(id);

	  if (!deletePet){
		return res.status(500).json({message: "No se ha podido eliminar la pelicula"});
	  }

	  return res.status(200).json('Movie deleted!');
	} catch (err) {
	  next(err);
	}
});





module.exports = router;